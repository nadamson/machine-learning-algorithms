# Machine Learning Jupyter Notebooks
### This is a collection of the Jupyter Notebooks I created during my internship at ATI metals.
### They each explain a different machine learning algorithm and how to impliment them using Python and Sci-kit Learn.

# Necessary Software:
### The files are Jupyter Notebooks so [Jupyter](https://jupyter.org/install.html) is a must and [Anaconda](https://www.anaconda.com/download/) is recommended.
### All of the notebooks are written in Python 3, but 3.5+ is preferable.

# Instructions:
### Using git in the command line/terminal, navigate to the directory you wish to add these files to and run the command:
### git clone https://gitlab.com/nadamson/machine-learning-notebooks.git
### Next, navigate to the folder using Jupyter and enjoy learning about machine learning algorithms!
#### * For some of the notebooks, you may need to install some new Python packages using your package manager of choice, this is where Anaconda comes in handy.
